//GUIA ESTRUCTURAS EJERCICIO 2
//Una aerolinea tiene vuelos, los cuales poseen un c�digo alfanum�rico (ejemplo: AR1500), una ciudad de origen y una ciudad de destino.
//Ingrese 4 vuelos en un vector. Luego de ingresados los datos permita que el usuario, escriba el nombre de una ciudad y muestre los vuelos que parten y los que arriban a esa ciudad.
//Si no hubiera vuelos para la ciudad ingresada debe mostrarse un mensaje de error.

#include <string.h>
#include <stdio.h>

struct vuelo

{
    char codigo[10];
    char partida[20];
    char destino[20];
};

//Declaro la estructura vuelo y los datos que incluira, estos en char ya que contienen numeros y letras.

int main()

{
    int i = 0;
    int vuelo_encontrado = 0;
    
//Agrego una flag para detectar si no se encuentra ningun vuelo y en ese caso mostrar un mensaje de error.
    
    char ciudad[20];
    struct vuelo vuelos[4];    
    
    for(i=0; i<4; i++)
	{
        printf("Ingrese el codigo de vuelo: ");
        scanf("%s", &vuelos[i].codigo);
        printf("Ingrese ciudad de partida: ");
        scanf("%s", &vuelos[i].partida);
        printf("Ingrese ciudad de destino: ");
        scanf("%s", &vuelos[i].destino);
    }
    
    printf("Ingrese la ciudad en la que busque vuelos: ");
    scanf("%s", ciudad);

    for(i=0; i<4; i++)
	{
        if(strcmp(ciudad, vuelos[i].partida) == 0)
        {
        	printf("Vuelo que parte de %s: %s\n", ciudad, vuelos[i].codigo);
        	vuelo_encontrado = 1;
        }
    }
    for(i=0; i<4; i++)
	{
        if(strcmp(ciudad, vuelos[i].destino) == 0)
		{
			printf("Vuelo que arriba a %s: %s\n", ciudad, vuelos[i].codigo);
            vuelo_encontrado = 1;
        }
    }
    if(vuelo_encontrado == 0)
    {
        printf("[ERROR] No hay ningun vuelo que parta de o arribe a la ciudad ingresada");
    }
return 0;
}